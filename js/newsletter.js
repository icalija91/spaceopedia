$(document).ready(function() {
  resetErrors();

  $("#formSubmit").click(function(e) {
    e.preventDefault();
    var email = $("#email").val();
    if (validEmail(email)) {
      $(".emailError").hide();
      toggleEmailError(false);
    } else {
      $(".emailError").show();
      toggleEmailError(true);
    }
    var name = $("#name").val();
    if (validName(name)) {
      $(".nameError").hide();
      toggleNameError(false);
    } else {
      $(".nameError").show();
      toggleNameError(true);
    }
    if (validForm(email, name)) {
      $("#email").val("");
      $("#name").val("");
      alert("You have been sucessfully added to the subscriber list!");
    }
  });
});

function validForm(email, name) {
  if (email.length != 0 && name.length != 0) {
    return true;
  }
  return false;
}

function validEmail(email) {
  if (email.length == 0) {
    return false;
  }
  return true;
}

function validName(name) {
  if (name.length == 0) {
    return false;
  }
  return true;
}

function resetErrors() {
  $(".emailError").hide();
  $(".nameError").hide();
  toggleNameError(false);
  toggleEmailError(false);
}

function toggleEmailError(showError) {
  if (showError) {
    $("#newsletter")
      .find("#email")
      .addClass("inputError");
  } else {
    $("#newsletter")
      .find("#email")
      .removeClass("inputError");
  }
}

function toggleNameError(showError) {
  if (showError) {
    $("#newsletter")
      .find("#name")
      .addClass("inputError");
  } else {
    $("#newsletter")
      .find("#name")
      .removeClass("inputError");
  }
}
